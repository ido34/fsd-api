# FSD API

## SQL Functions

### Introduction

There are two types of sql functions:
* Dashboard SQL functions under the `db/functions` directory
* Job SQL functions under the `db/jobs` directory

### Dashboard SQL Functions

#### Agenda

* SQL functions on `db/functions` are meant to be used directly by the dashboard (on-demand) and should have super fast execution time
* In most use cases a function will "select" a pre aggregated table but it's possible to expose a function that will return raw data from a normal table

#### Guidelines

* Almost always a dashboard SQL function should accept `fleet_id`
* For returning time based data a function should accept time arguments (`start_data` & `end_date`)
* SQL function argument names should start with `arg_*`
* The SQL function name should start with `get_*`

#### Example

`get_ships_by_risk.sql:`
```sql
CREATE OR REPLACE FUNCTION fsd.get_ships_by_risk(arg_fleet_id int, arg_start_date timestamp without time zone, arg_end_date timestamp without time zone)
RETURNS TABLE (fleet_id int, severity_level text, num_of_ships int, start_time timestamp without time zone, end_time timestamp without time zone) AS 
$$
BEGIN  

	RETURN QUERY 
		SELECT pa.fleet_id, pa.severity_level, pa.num_of_ships, pa.start_time, pa.end_time
		FROM fsd.pre_aggregated_ships_by_risk as pa
		WHERE pa.fleet_id = (arg_fleet_id) AND pa.start_time >= (arg_start_date) AND pa.end_time <= (arg_end_date);

END;
$$ LANGUAGE plpgsql;

select * from fsd.get_ships_by_risk(9, '2022-12-13', '2022-12-15')
```

### Pre aggregations

#### Agenda

* SQL functions on `db/jobs` are executed on interval basis
* A job should be executed on a range of dates (`start_time` and `end_time`)
* A job will append data to a pre aggregated table

#### Guidelines

* A job directory should contain two SQL files - `create_table.sql` and `job.sql`
  * `create_table.sql` should create a dedicated pre aggregated table with a prefix of  `pre_aggregated_*` (for example `pre_aggregated_ships_by_risk`)
  * `job.sql` will create the SQL function that will be executed regularly
* A job function name should always start with `job_*`
* A pre aggregated table should always have the following columns:
  * `table_name_id` (primary key auto increment)
  * `fleet_id` (foreign key)
  * `created_at` (timestamp without time zone default now)
* `job.sql` should not accept any input parameters
* `job.sql` should not return anything (`RETURNS VOID`)
* `job.sql` should be executed on all fleets and have a `GROUP BY` expression on `fleet_id` 
* `job.sql` should process data starting the last execution time (`created_at`) of the job till "now"

#### Example
`create_table.sql:`
```sql
CREATE TABLE fsd.pre_aggregated_ships_by_risk
(
  pre_aggregated_ships_by_risk_id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()::timestamp,
  fleet_id INT REFERENCES public.fleets(fleet_id),
  start_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  severity_level TEXT,
  num_of_ships INTEGER
)
```

`job.sql`
```sql
CREATE OR REPLACE FUNCTION fsd.job_ships_by_risk()
RETURNS VOID AS
$$
  DECLARE last_execution_time TIMESTAMP WITHOUT TIME ZONE;
BEGIN

	last_execution_time = COALESCE (
		(
			SELECT created_at 
			FROM fsd.pre_aggregated_ships_by_risk 
			ORDER BY created_at DESC
			LIMIT 1
		),
		'2022-12-14'
	);
	
	INSERT INTO fsd.pre_aggregated_ships_by_risk (fleet_id, start_time, end_time, severity_level, num_of_ships)
	(
		SELECT i.fleet_id, last_execution_time, NOW()::TIMESTAMP, i.severity_level, COUNT(*) AS num_of_ships 
		FROM 
			(
				SELECT s.fleet_id, ds.date, own_ship_id, AVG(score) AS avg_score_per_ship,
				CASE 
					WHEN score < 4 THEN 'High Risk'
					WHEN score >= 4 AND score <= 7 THEN 'Medium Risk'
					WHEN score > 7 THEN 'Low Risk'
				END severity_level
				FROM fleet_safety_dashboard.daily_score_ships ds JOIN ships s ON ds.own_ship_id = s.ship_id
				WHERE date = DATE('2022-09-26') 
				GROUP BY s.fleet_id, ds.date, own_ship_id, score
			) i
		GROUP BY i.fleet_id, i.date, severity_level
	);

END; 
$$ LANGUAGE plpgsql;
```

### API

#### Agenda
* Every SQL function will be exposed by the API by a static python function.
* Function get arguments and adapt them to the configuration that the SQL function expects to receive.

#### Guidlined
* Function name have to be exactly as the SQL function name.
* Every function declare the `schema` to read from, `function` to execute and the arguments of the SQL function.

#### Example
`postgres_queries.py`
```
@staticmethod
def get_ships_by_risk(schema: str, function: str, fleet_id, start_date, end_date) -> str:
    start_date = f"'{start_date}'"
    end_date = f"'{end_date}'"
    args =','.join((fleet_id, start_date, end_date))
    return PostgresQueries.select_view(schema, function, args)
```