import asyncio
from logging import Logger

from src import CoreModule
from src import ViewsService
from injector import Injector
from dotenv import load_dotenv

load_dotenv()

def main(event: dict, context):
    """
    Fleet Safety Dashboard Service trigger.
    """
    ioc_container = Injector(modules=[CoreModule])

    logger = ioc_container.get(Logger)
    logger.info("Starting fleet-safety-dashboard lambda")

    views_service = ioc_container.get(ViewsService)
    logger.info("Get ships data")
    response = asyncio.get_event_loop().run_until_complete(views_service.execute_view(event))

    logger.info("Send response")
    return response