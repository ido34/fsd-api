from typing import List

class PostgresQueries:
    @staticmethod
    def select_view(schema: str, function: str, arguments: str) -> str:
        postgreSQL_select_Query = f'SELECT * FROM {schema}.{function}({arguments})'
        return postgreSQL_select_Query

    @staticmethod
    def get_ships(schema: str, function: str, fleet_id: int) -> str:
        args =','.join((fleet_id))
        return PostgresQueries.select_view(schema, function, args)
    
    @staticmethod
    def ships_by_risk(schema: str, function: str, fleet_id: int, end_date: str) -> str:
        args =','.join((fleet_id, end_date))
        return PostgresQueries.select_view(schema, function, args)