import json

class InputService:
    @staticmethod
    def is_authorization_validate(event: dict) -> bool:
        if ('claims' not in event['requestContext']['authorizer']['jwt'] and 
              'role' not in event['requestContext']['authorizer']['jwt']['claims']):
            return False
        
        if (event['requestContext']['authorizer']['jwt']['claims']['role'] != 'orca-admin'):
            if 'fleetId' not in event['requestContext']['authorizer']['jwt']['claims']:
                return False
        
        return True

    @staticmethod
    def get_input_args(event: dict):
        arguments = {}
        if event['requestContext']['authorizer']['jwt']['claims']['role'] != 'orca-admin':
            fleet_id = json.dumps({'fleet_id': event['requestContext']['authorizer']['jwt']['claims']['fleetId']})
            arguments.update(json.loads(fleet_id))
        if 'queryStringParameters' in event:
            query_params = json.dumps(event['queryStringParameters'])
            arguments.update(json.loads(query_params))
        return arguments