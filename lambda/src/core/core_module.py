import os
from logging import DEBUG, INFO, Logger

from injector import Module, provider, singleton
from orcaai_core import AsyncPostgresClient, get_git_hash_version
from orcaai_core.logging import LoggingFormat, LoggingOutput, get_logger


class CoreModule(Module):
    @singleton
    @provider
    def provide_postgres_client(self) -> AsyncPostgresClient:
        postgres_url = os.getenv("POSTGRES_URL")
        dsn_auth, _, dsn_hostspec = postgres_url.rpartition('@')
        if ('@' in dsn_auth):
            dsn_auth = dsn_auth.replace('@', '%40')
        postgres_url = "@".join([dsn_auth,dsn_hostspec])

        postgres_conn_pool_size = int(os.getenv("POSTGRES_CONN_POOL_SIZE", "1"))

        postgres_client = AsyncPostgresClient(postgres_url, postgres_conn_pool_size)
        return postgres_client

    @singleton
    @provider
    def provide_logger(self) -> Logger:
        log_level = INFO
        if os.getenv("LOG_LEVEL", "INFO") == "DEBUG":
            log_level = DEBUG

        logger = get_logger(
            logger_name="get_view",
            logger_version=get_git_hash_version(),
            logger_format=LoggingFormat.JSON,
            logger_level=log_level,
            logger_output=LoggingOutput.STDOUT,
        )
        return logger
