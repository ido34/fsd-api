import os
import json

from orcaai_core import AsyncPostgresClient

from logging import Logger
from http import HTTPStatus
from .queries import PostgresQueries
from .validation import InputService
from injector import inject, singleton

@singleton
class ViewsService:
    @inject
    def __init__(self, logger: Logger, postgres_client: AsyncPostgresClient):
        self.postgres_client = postgres_client
        self.logger = logger

    async def connect(self):
        await self.postgres_client.connect()

    async def disconnect(self) -> None:
        if self.postgres_client:
            await self.postgres_client.close()

    async def _execute_view(self, args: dict) -> None:
        schema = os.getenv("SCHEMA")
        function_to_execute = os.getenv("FUNCTION")
        query_to_execute = getattr(PostgresQueries, function_to_execute)
        get_query = query_to_execute(schema, function_to_execute, **args)
        result = await self.postgres_client.select_many(get_query)
        return result 

    async def execute_view(self, event: dict):
        try:
            try:
                await self.connect()
            except Exception as ex: 
                error_message = f"Error while trying connect to db. {ex}"
                self.logger.error(error_message)
                return {
                "statusCode": HTTPStatus.INTERNAL_SERVER_ERROR.value,
                "body": {"message": error_message}
            }

            if InputService.is_authorization_validate(event) == False:
                error_message = "Error while connecting to db!"
                self.logger.error(error_message)
                return {"statusCode": HTTPStatus.UNAUTHORIZED.value, "body": {"message": error_message}}
            
            arguments = InputService.get_input_args(event)

            try:
                response = await self._execute_view(arguments)
            except Exception as ex: 
                error_message = f"Error while trying execute view. {ex}"
                self.logger.error(error_message)
                return {
                "statusCode": HTTPStatus.INTERNAL_SERVER_ERROR.value,
                "body": {"message": error_message}
            }

            if not response:
                error_message = "Resource not found!"
                self.logger.error(error_message)
                return {"statusCode": HTTPStatus.NOT_FOUND.value, "body": {"message": error_message}}

            return { "statusCode": HTTPStatus.OK.value, "body": json.dumps(response) }
            
        except Exception as ex: 
            error_message = f"Error while trying execute query. {ex}"
            self.logger.error(error_message)
            return {
                "statusCode": HTTPStatus.INTERNAL_SERVER_ERROR.value,
                "body": error_message
            }
        finally:
            await self.disconnect()