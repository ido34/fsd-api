CREATE OR REPLACE FUNCTION fsd.job_ships_by_risk()
RETURNS VOID AS
$$
  DECLARE last_execution_time TIMESTAMP WITHOUT TIME ZONE;
BEGIN

	last_execution_time = COALESCE (
		(
			SELECT created_at 
			FROM fsd.pre_aggregated_ships_by_risk 
			ORDER BY created_at DESC
			LIMIT 1
		),
		'2022-12-14'
	);
	
	INSERT INTO fsd.pre_aggregated_ships_by_risk (fleet_id, start_time, end_time, severity_level, num_of_ships)
	(
		SELECT i.fleet_id, last_execution_time, NOW()::TIMESTAMP, i.severity_level, COUNT(*) AS num_of_ships 
		FROM 
			(
				SELECT s.fleet_id, ds.date, own_ship_id, AVG(score) AS avg_score_per_ship,
				CASE 
					WHEN score < 4 THEN 'High Risk'
					WHEN score >= 4 AND score <= 7 THEN 'Medium Risk'
					WHEN score > 7 THEN 'Low Risk'
				END severity_level
				FROM fleet_safety_dashboard.daily_score_ships ds JOIN ships s ON ds.own_ship_id = s.ship_id
				WHERE date = DATE('2022-09-26') 
				GROUP BY s.fleet_id, ds.date, own_ship_id, score
			) i
		GROUP BY i.fleet_id, i.date, severity_level
	);

END; 
$$ LANGUAGE plpgsql;