CREATE TABLE fsd.pre_aggregated_ships_by_risk
(
  pre_aggregated_ships_by_risk_id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()::timestamp,
  fleet_id INT REFERENCES public.fleets(fleet_id),
  start_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  severity_level TEXT,
  num_of_ships INTEGER
)