CREATE TABLE fsd.pre_aggregated_latest_highest_risk_events
(
  pre_aggregated_latest_highest_risk_events_id BIGSERIAL PRIMARY KEY,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()::timestamp,
  fleet_id INT REFERENCES public.fleets(fleet_id),
  start_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  end_time TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  ship_name TEXT,
  alert_name TEXT,
  severity_level TEXT,
  timestamp_start TIMESTAMP WITHOUT TIME ZONE, 
  geo_tag TEXT,
  own_sog_start NUMERIC,
  ais_target_sog_start NUMERIC,
  ais_target_distance_min_dist NUMERIC,
  max_rot NUMERIC,
  max_delta_sog NUMERIC,
  max_delta_roll NUMERIC,
  max_delta_pitch NUMERIC,
  min_depth NUMERIC
);