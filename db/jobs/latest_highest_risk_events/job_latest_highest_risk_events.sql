CREATE OR REPLACE FUNCTION fsd.job_latest_highest_risk_events(arg_start_date timestamp without time zone, arg_end_date timestamp without time zone)
RETURNS VOID AS
$$
  DECLARE last_execution_time TIMESTAMP WITHOUT TIME ZONE;
BEGIN

	last_execution_time = COALESCE (
		(
			SELECT created_at 
			FROM fsd.pre_aggregated_latest_highest_risk_events 
			ORDER BY created_at DESC
			LIMIT 1
		),
		'2022-12-14'
	);

INSERT INTO fsd.pre_aggregated_latest_highest_risk_events (fleet_id, start_time, end_time, severity_level, ship_name, alert_name, timestamp_start, 
														   geo_tag, own_sog_start, ais_target_sog_start, ais_target_distance_min_dist,max_rot, 
														   max_delta_sog, max_delta_roll, max_delta_pitch, min_depth)
(		
	SELECT fleet_id, last_execution_time, NOW()::TIMESTAMP, severity_level, ship_name, alert_name, timestamp_start,
			geo_tag, own_sog_start, ais_target_sog_start, ais_target_distance_min_dis, max_rot,
			max_delta_sog, max_delta_roll, max_delta_pitch, min_depth
	FROM
	(
	SELECT DISTINCT s.fleet_id, NOW()::TIMESTAMP, sce.severity_level, s.ship_name, a.alert_name, ce.timestamp_start, ce.geo_tag, ce.own_sog_start, ce.ais_target_sog_start, ce.ais_target_distance_min_dis,  
			ce.max_rot, ce.max_delta_sog, ce.max_delta_roll, ce.max_delta_pitch, ce.min_depth,
	rank() OVER  (PARTITION BY s.fleet_id ORDER BY ce.timestamp_start DESC, sce.severity_level DESC)
	FROM fleet_safety_dashboard.clean_events_with_visual_available ce JOIN ships s ON ce.own_ship_id = s.ship_id
																	  JOIN alert_types a ON ce.alert_enum = a.alert_id
																	  JOIN fleet_safety_dashboard.clean_events_metadata sce 
																		ON ce.id = sce.event_id AND ce.own_ship_id = sce.own_ship_id 


	WHERE ce.timestamp_start >= arg_start_date AND ce.timestamp_start <= arg_end_date
	) ranked
WHERE rank <= 5
);

END; 
$$ LANGUAGE plpgsql;