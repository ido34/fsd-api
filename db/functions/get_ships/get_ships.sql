CREATE OR REPLACE FUNCTION get_ships(in_fleet_id int) 
RETURNS TABLE (ship_id int, host_name varchar) AS 
$$
  SELECT ship_id, host_name FROM public.ships
  WHERE fleet_id = in_fleet_id;
$$ LANGUAGE SQL;