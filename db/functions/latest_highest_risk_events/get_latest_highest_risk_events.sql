CREATE OR REPLACE FUNCTION fsd.get_latest_highest_risk_events(arg_fleet_id int, arg_start_date timestamp without time zone, arg_end_date timestamp without time zone)
RETURNS TABLE (
				fleet_id INT, ship_name TEXT, alert_name TEXT, timestamp_start TIMESTAMP WITHOUT TIME ZONE, geo_tag TEXT, own_sog_start NUMERIC, 
				ais_target_sog_start NUMERIC, ais_target_distance_min_dis NUMERIC,  max_rot NUMERIC, max_delta_sog NUMERIC, max_delta_roll NUMERIC,
				max_delta_pitch NUMERIC, min_depth NUMERIC, start_time TIMESTAMP WITHOUT TIME ZONE, end_time TIMESTAMP WITHOUT TIME ZONE
			) AS
$$
BEGIN

	RETURN QUERY 
			SELECT pa.fleet_id, pa.ship_name, pa.alert_name, pa.timestamp_start, pa.geo_tag, pa.own_sog_start, pa.ais_target_sog_start, pa.ais_target_distance_min_dist,
						pa.max_rot, pa.max_delta_sog, pa.max_delta_roll, pa.max_delta_pitch, pa.min_depth, pa.start_time, pa.end_time
			FROM fsd.pre_aggregated_latest_highest_risk_events pa
			WHERE pa.fleet_id = (arg_fleet_id) AND pa.start_time >= (arg_start_date) AND pa.end_time <= (arg_end_date)
			ORDER BY timestamp_start DESC, severity_level DESC
			LIMIT 5;

END;
$$ LANGUAGE plpgsql;