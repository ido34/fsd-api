CREATE OR REPLACE FUNCTION fsd.get_ships_by_risk(arg_fleet_id int, arg_start_date timestamp without time zone, arg_end_date timestamp without time zone)
RETURNS TABLE (fleet_id int, severity_level text, num_of_ships int, start_time timestamp without time zone, end_time timestamp without time zone) AS 
$$
BEGIN  

	RETURN QUERY 
		SELECT pa.fleet_id, pa.severity_level, pa.num_of_ships, pa.start_time, pa.end_time
		FROM fsd.pre_aggregated_ships_by_risk as pa
		WHERE pa.fleet_id = (arg_fleet_id) AND pa.start_time >= (arg_start_date) AND pa.end_time <= (arg_end_date);

END;
$$ LANGUAGE plpgsql;
